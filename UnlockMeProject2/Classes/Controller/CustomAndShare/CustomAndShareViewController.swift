//
//  CustomAndShareViewController.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/22/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

class CustomAndShareViewController: UIViewController {
    //MARK: - IBOutlet Property
    @IBOutlet weak var currentImageView: UIImageView!

    //MARK: - Property
    var currentImage: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        if currentImage != nil {
            self.currentImageView.image = currentImage

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Function
    
    
    //MARK: - IBOutlet Action
    
    @IBAction func onCustomImage(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let selectPuzzleSizeController = storyboard.instantiateViewController(withIdentifier: "SelectPuzzleSizeViewController") as! SelectPuzzleSizeViewController
        selectPuzzleSizeController.currentImage = self.currentImage
        self.navigationController?.pushViewController(selectPuzzleSizeController, animated: true)
    }
    
    @IBAction func onShareImage(_ sender: Any) {
    }

    @IBAction func onBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onOpenLeftMenu(_ sender: Any) {
    }

}
