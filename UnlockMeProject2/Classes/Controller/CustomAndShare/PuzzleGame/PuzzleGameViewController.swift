//
//  PuzzleGameViewController.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/23/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage
import FacebookShare
import FBSDKShareKit

private let EMPTY_BOX_VALUE = -1
private let cropFailureImage = UIImage()

enum Trend {
    case left, right, up, down, none
}


class PuzzleGameViewController: UIViewController {
    //MARK: - IBOutlet Property
    @IBOutlet weak var naviImage: UIImageView!
    @IBOutlet weak var gameView: UIView!
    @IBOutlet weak var naviImageWidthConstraint: NSLayoutConstraint!
    // game info
    @IBOutlet weak var timeValueLbl: UILabel!
    @IBOutlet weak var scoreValueLbl: UILabel!
    @IBOutlet weak var countOfMoveValueLbl: UILabel!
    //MARK: - Property
    var currentImage: UIImage!
    var numberOfPieces: Int!
    var haveNumberInPieces = false
    var maxOfScore: Int!
    
    private var arrSubImages: [UIImage] = []
    private var arrImageViews: [UIImageView] = []
    private var arrIndexMap: [Int] = []             // -1 is empty box
    private var numberOfPiecesInLine = 0
    
    // Timer
    private var timer: Timer?
    private var timeValue = 0
    private var countMove = 0
    private var currentScore: Int!
    //MARK: - Gesture
    
    //MARK: - System Fuction
    override func viewDidLoad() {
        super.viewDidLoad()
        if currentImage != nil {
            //self.naviImage.image = self.currentImage
        }
        self.currentScore = self.maxOfScore
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTheme()
        self.splitCurrentImage()
        DispatchQueue.main.async {
            self.configGame()
            self.addGestureForGameView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.creatTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimer()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Function
    func setTheme() {
        DispatchQueue.main.async {
            self.naviImageWidthConstraint.constant = self.naviImage.frame.size.height * self.currentImage.size.width / self.currentImage.size.height
        }
        
    }
    
    func splitCurrentImage() {
        self.arrSubImages.removeAll()
        self.numberOfPiecesInLine = Int(sqrt(Double(self.numberOfPieces)))

        let currentimageSize = self.currentImage.size
        
        let subImageWidth = floor(currentimageSize.width / CGFloat(self.numberOfPiecesInLine))
        let subImageHeight = floor(currentimageSize.height / CGFloat(self.numberOfPiecesInLine))
        
        let currentCGImage = self.currentImage.cgImage
        
        for i in 0..<numberOfPieces {
            let subImageX = floor(CGFloat(i % numberOfPiecesInLine) * subImageWidth)
            let subImageY = floor(CGFloat(Int(i / numberOfPiecesInLine)) * subImageHeight)

            let subImageRect = CGRect(x: subImageX, y: subImageY, width: subImageWidth, height: subImageHeight)
            
            let subImageCIImage = currentCGImage?.cropping(to: subImageRect)
            if subImageCIImage != nil {
                let subImage = UIImage(cgImage: subImageCIImage!, scale: self.currentImage.scale, orientation: .up)
                
                // if have number in peieces
                if self.haveNumberInPieces {
                   let newSubImage = AppManager.sharedInstance.textToImage(drawText: "\(i + 1)" as NSString, inImage: subImage, atPoint: CGPoint.zero)
                    self.arrSubImages.append(newSubImage)
                } else {
                    self.arrSubImages.append(subImage)
                }
                
            } else {
                self.arrSubImages.append(cropFailureImage)
            }
            
        }
        
    }
    
    func mergeImage() -> UIImage{
        let newImageSize = self.currentImage.size
        let subImageWidth = floor(newImageSize.width / CGFloat(self.numberOfPiecesInLine))
        let subImageHeight = floor(newImageSize.height / CGFloat(self.numberOfPiecesInLine))
        
        UIGraphicsBeginImageContext(newImageSize)
        for i in 0..<self.numberOfPieces {
            let subImageX = floor(CGFloat(i % numberOfPiecesInLine) * subImageWidth)
            let subImageY = floor(CGFloat(Int(i / numberOfPiecesInLine)) * subImageHeight)
            let subImageRect = CGRect(x: subImageX, y: subImageY, width: subImageWidth, height: subImageHeight)
            
            var img: UIImage!
            let subImgIndex = arrIndexMap[i]
            
            if subImgIndex == -1 {
                img = arrSubImages[self.numberOfPiecesInLine - 1]
            } else {
                img = arrSubImages[subImgIndex]
            }
            
            img.draw(in: subImageRect)
        }
        
        let result: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return result
    }
    
    func configGame() {
        self.arrImageViews.removeAll()
        let gameViewSize = self.gameView.bounds.size
        
        let imgVHeight = gameViewSize.height / CGFloat(numberOfPiecesInLine)
        let imgVWidth = gameViewSize.width / CGFloat(numberOfPiecesInLine)
        
        for i in 0..<self.numberOfPieces {
            if i != self.numberOfPiecesInLine - 1 {
                
                let imgVOriginX = CGFloat(i % numberOfPiecesInLine) * imgVWidth
                let imgVOriginY = CGFloat(Int(i / numberOfPiecesInLine)) * imgVHeight
                
                let imgRect = CGRect(x: imgVOriginX, y: imgVOriginY, width: imgVWidth, height: imgVHeight)
                
                let imgV = UIImageView(frame: imgRect)
                imgV.backgroundColor = UIColor.red
                if self.arrSubImages[i] != cropFailureImage {
                    imgV.image = self.arrSubImages[i].scaleImage(toSize: imgRect.size)
                }
                imgV.contentMode = .scaleToFill
                
                imgV.layer.borderColor = UIColor.white.cgColor
                imgV.layer.borderWidth = 1.0
                
                self.arrImageViews.append(imgV)
                self.gameView.addSubview(imgV)
                print("imageview -- \(imgV.frame)")
                self.arrIndexMap.append(i)
                
            } else {
                self.arrIndexMap.append(EMPTY_BOX_VALUE)
                self.arrImageViews.append(UIImageView())
            }
            
        }
    }
    
    func addGestureForGameView() {
        // tap gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(sender:)))
        tapGesture.delegate = self
        tapGesture.numberOfTapsRequired = 1
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapGesture(sender:)))
        doubleTap.numberOfTapsRequired = 2
        
        self.gameView.addGestureRecognizer(tapGesture)
        self.gameView.addGestureRecognizer(doubleTap)
        tapGesture.require(toFail: doubleTap)
        
        // pan gesture
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(sender:)))
        panGesture.delegate = self
        self.gameView.addGestureRecognizer(panGesture)
    }
    
    func handlePanGesture(sender: UIPanGestureRecognizer?) {
        if sender?.state == .began {
            let point = sender!.location(in: self.gameView)
            let gameViewSize = self.gameView.bounds.size
            
            let subImgVWidth = gameViewSize.width / CGFloat(self.numberOfPiecesInLine)
            let subImgVHeight = gameViewSize.height / CGFloat(self.numberOfPiecesInLine)
            
            let indexX = Int(point.x / subImgVWidth)
            let indexY = Int(point.y / subImgVHeight)
            
            let subImgSelectIndex = indexY * self.numberOfPiecesInLine + indexX
            
            self.moveImgV(atIndex: subImgSelectIndex)
        }
    }
    
    func handleTapGesture(sender: UITapGestureRecognizer?) {
        let point = sender!.location(in: self.gameView)
        
        let gameViewSize = self.gameView.bounds.size
        
        let subImgVWidth = gameViewSize.width / CGFloat(self.numberOfPiecesInLine)
        let subImgVHeight = gameViewSize.height / CGFloat(self.numberOfPiecesInLine)
        
        let indexX = Int(point.x / subImgVWidth)
        let indexY = Int(point.y / subImgVHeight)
        
        let subImgSelectIndex = indexY * self.numberOfPiecesInLine + indexX
        
        self.moveImgV(atIndex: subImgSelectIndex)
        
    }
    
    func moveImgV(atIndex index: Int) {
        let trendToMove = self.trendToMoveImgV(atIndex: index)
        if trendToMove == .none {
            return
        }
        
        let imgIndex = self.arrIndexMap[index]
        let imgV = self.arrImageViews[imgIndex]
        if trendToMove == .up {
            UIView.animate(withDuration: 0.2, animations: {
                self.gameView.isUserInteractionEnabled = false
                imgV.frame.origin.y -= imgV.frame.size.height
            })  { (completion) in
                if completion {
                    self.arrIndexMap[index - self.numberOfPiecesInLine] = self.arrIndexMap[index]
                    self.arrIndexMap[index] = EMPTY_BOX_VALUE
                    print("arrIndexMap -- \(self.arrIndexMap)")
                    self.gameView.isUserInteractionEnabled = true
                    self.updateCountOfMove()
                }
            }

        } else if trendToMove == .down {
            UIView.animate(withDuration: 0.2, animations: {
                self.gameView.isUserInteractionEnabled = false
                imgV.frame.origin.y += imgV.frame.size.height
            }) { (completion) in
                if completion {
                    self.arrIndexMap[index + self.numberOfPiecesInLine] = self.arrIndexMap[index]
                    self.arrIndexMap[index] = EMPTY_BOX_VALUE
                    print("arrIndexMap -- \(self.arrIndexMap)")
                    self.gameView.isUserInteractionEnabled = true
                    self.updateCountOfMove()
                }
            }
        } else if trendToMove == .left {
            UIView.animate(withDuration: 0.2, animations: {
                self.gameView.isUserInteractionEnabled = false
                imgV.frame.origin.x -= imgV.frame.size.width
            }) { (completion) in
                if completion {
                    self.arrIndexMap[index - 1] = self.arrIndexMap[index]
                    self.arrIndexMap[index] = EMPTY_BOX_VALUE
                    print("arrIndexMap -- \(self.arrIndexMap)")
                    self.gameView.isUserInteractionEnabled = true
                    self.updateCountOfMove()
                }
            }
        } else if trendToMove == .right {
            UIView.animate(withDuration: 0.2, animations: {
                self.gameView.isUserInteractionEnabled = false
                imgV.frame.origin.x += imgV.frame.size.width
            }) { (completion) in
                if completion {
                    self.arrIndexMap[index + 1] = self.arrIndexMap[index]
                    self.arrIndexMap[index] = EMPTY_BOX_VALUE
                    print("arrIndexMap -- \(self.arrIndexMap)")
                    self.gameView.isUserInteractionEnabled = true
                    self.updateCountOfMove()
                }
            }
        }
        
        
    }
    
    func trendToMoveImgV(atIndex index: Int) -> Trend {
        
        if arrIndexMap[index] == EMPTY_BOX_VALUE {
            return .none
        }
        
        if index % numberOfPiecesInLine != 0 {
            if self.arrIndexMap[index - 1] == EMPTY_BOX_VALUE {
                return Trend.left
            }
        }
        
        if (index - self.numberOfPiecesInLine) >= 0 {
            if self.arrIndexMap[index - self.numberOfPiecesInLine] == EMPTY_BOX_VALUE {
                return Trend.up
            }
        }
        
        if index + self.numberOfPiecesInLine < self.numberOfPieces {
            if self.arrIndexMap[index + self.numberOfPiecesInLine] == EMPTY_BOX_VALUE {
                return Trend.down
            }
        }
        
        if (index + 1) % self.numberOfPiecesInLine != 0 {
            if self.arrIndexMap[index + 1] == EMPTY_BOX_VALUE {
                return Trend.right
            }
        }
        
        return Trend.none
    }
    
    func creatTimer() {
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
           self.timeValue += 1
            self.timeValueLbl.text = "\(self.timeValue)"
        })
    }
    
    func stopTimer() {
        self.timer?.invalidate()
    }
    
    func updateCountOfMove() {
        self.countMove += 1
        self.countOfMoveValueLbl.text = "\(self.countMove)"
    }

    //MARK: - IBOutlet Action
    
    @IBAction func onShare(_ sender: Any) {
        let newImg: UIImage = self.mergeImage()
        self.naviImage.image = newImg
        
        let photo : FBSDKSharePhoto = FBSDKSharePhoto(image: newImg, userGenerated: true)
        let content : FBSDKSharePhotoContent = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        FBSDKShareDialog.show(from: self, with: content, delegate: nil)
    }
    
    @IBAction func onOpenLeftMenu(_ sender: Any) {
    }
    
    @IBAction func onBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - Gesture Delegate

extension PuzzleGameViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
