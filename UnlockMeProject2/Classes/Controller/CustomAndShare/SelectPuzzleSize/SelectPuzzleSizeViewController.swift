//
//  SelectPuzzleSizeViewController.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/22/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

private let reuseIdentifier = "SelectPuzzleSizeCollectionCell"
class SelectPuzzleSizeViewController: UIViewController {
    //MARK: - IBOutlet Property
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK: - Property
    var currentImage: UIImage!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configCollectionView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Function
    func configCollectionView() {
        let nib = UINib(nibName: reuseIdentifier, bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier)
    }
    
    //MARK: - IBOutlet Action
    @IBAction func onBack(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

    @IBAction func onOpenLeftMenu(_ sender: Any) {
    }
}

// MARK: CollectionView DELEGATE, DATASOURCE, FLOW LAYOUT

extension SelectPuzzleSizeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! SelectPuzzleSizeCollectionCell
        if self.currentImage != nil {
            cell.imgView.image = self.currentImage
        }
        
        if indexPath.row % 2 == 1 {
            cell.haveNumber = true
        } else {
            cell.haveNumber = false
        }
        
        cell.numberSquareInLine = 3 + Int(indexPath.row / 2)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = SCREEN_WIDTH / 2 - 20
        return CGSize(width: cellWidth, height: cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let puzzleGameController = storyboard.instantiateViewController(withIdentifier: "PuzzleGameViewController") as! PuzzleGameViewController
        let numberOfPiecesInLine = 3 + Int(indexPath.row / 2)
        puzzleGameController.currentImage = self.currentImage
        puzzleGameController.numberOfPieces = numberOfPiecesInLine * numberOfPiecesInLine
        puzzleGameController.haveNumberInPieces = (Int(indexPath.row) % 2 == 1) ? true : false
        
        self.navigationController?.pushViewController(puzzleGameController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 8, left: 10, bottom: 8, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
}
