//
//  SelectPuzzleSizeCollectionCollectionCell.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/23/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

class SelectPuzzleSizeCollectionCollectionCell: UICollectionViewCell {

    @IBOutlet weak var sttLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func creatShadowForCell() {
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 0.5
    }

}
