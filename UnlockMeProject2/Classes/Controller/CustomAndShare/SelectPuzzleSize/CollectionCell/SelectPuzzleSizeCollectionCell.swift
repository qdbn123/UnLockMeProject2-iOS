//
//  SelectPuzzleSizeCollectionCell.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/22/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

private let reuseIdentifier2 = "SelectPuzzleSizeCollectionCollectionCell"

class SelectPuzzleSizeCollectionCell: UICollectionViewCell {
    //MARK: - IBOutlet Property
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Property
    var haveNumber = false
    var numberSquareInLine = 3

    override func awakeFromNib() {
        super.awakeFromNib()
        self.configCollectionView()
    }
    
    func configCollectionView() {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.backgroundColor = UIColor.clear
        self.collectionView.isScrollEnabled = false
        collectionView.isUserInteractionEnabled = false
        let nib = UINib(nibName: reuseIdentifier2, bundle: nil)
        self.collectionView.register(nib, forCellWithReuseIdentifier: reuseIdentifier2)
    }
}

//MARK: - CollecitonView DELEGATE, DATASOURCE, FLOWLAYOUT

extension SelectPuzzleSizeCollectionCell: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberSquareInLine * numberSquareInLine
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier2, for: indexPath) as! SelectPuzzleSizeCollectionCollectionCell
        cell.creatShadowForCell()
        cell.backgroundColor = UIColor.clear
        
        if self.haveNumber {
            cell.sttLbl.text = "\(indexPath.row + 1)"
        } else {
            cell.sttLbl.text = ""
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = (SCREEN_WIDTH / 2 - 20) / CGFloat(numberSquareInLine)
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
