//
//  RightMenuViewController.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/25/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKCoreKit
import FBSDKLoginKit
import SDWebImage

class RightMenuViewController: UIViewController {
    //MARK: - IBOutlet Property
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var facebookNameLbl: UILabel!
    
    @IBOutlet weak var fbThumnailImgV: UIImageView!
    
    //MARK: - Property

    
    // MARK: - System Function
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setTheme()
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Function
    func setTheme() {
        for subView in self.scrollView.subviews {
            subView.layer.cornerRadius = 15.0
        }
    }
    
    func updateProfile() {
        if Global.user != nil {
            self.facebookNameLbl.text = Global.user!.name
            self.fbThumnailImgV.sd_setImage(with: URL(string: (Global.user!.thumnail)), placeholderImage: UIImage(named: "ic_account"))
        }
    }

    // MARK: - IBOutlet Action
    @IBAction func onBack(_ sender: Any) {
       _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onLoginFaceBook(_ sender: Any) {
        let loginManager = LoginManager()
        loginManager.logIn([.publicProfile], viewController: self) { (loginResult) in
            switch loginResult {
            case .failed(let error):
                print(error)
                break
            case .cancelled:
                print("User Cancel")
                break
            case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                self.view.makeToast("Login success", duration: 2.0, position: .center)
                // get profile
                FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, email, picture.type(large)"]).start(completionHandler: { (connection, result, error) -> Void in
                    if (error == nil){
                        if let dicData = result as? Dictionary <String, Any> {
                            Global.user = User(withDic: dicData)
                            self.updateProfile()
                        }
                    }else{
                        print(error?.localizedDescription ?? "Not found")
                    }
                })
                
                break
                
            }
        }
    }

}
