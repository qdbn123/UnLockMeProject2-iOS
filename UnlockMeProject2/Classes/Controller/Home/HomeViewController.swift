//
//  HomeViewController.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/22/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController {
    // MARK: - IBOutlet property
    @IBOutlet weak var leftMenuBtn: UIButton!
    @IBOutlet weak var galleryBtn: UIButton!
    @IBOutlet weak var takePhotoBtn: UIButton!
    
    // MARK: - Property
    var imagePicker = UIImagePickerController()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setTheme()
        self.customImagePicker()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Funtion
    
    func setTheme() {
        //btn gallery
        self.galleryBtn.layer.cornerRadius = 10
        
        //btn take photo
        self.takePhotoBtn.layer.cornerRadius = 10
    }
    
    func customImagePicker() {
        self.imagePicker.allowsEditing = false
        self.imagePicker.delegate = self
    }
    

    
    // MARK: - IBOutlet Action
    
    @IBAction func onOpenLeftMenu(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let rightController = storyboard.instantiateViewController(withIdentifier: "RightMenuViewController") as! RightMenuViewController
        self.navigationController?.pushViewController(rightController, animated: true)
    }

    @IBAction func onGallery(_ sender: Any) {
        self.imagePicker.sourceType = .photoLibrary
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func onTakePhoto(_ sender: Any) {
        self.imagePicker.sourceType = .camera
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            self.present(imagePicker, animated: true, completion: nil)
        }

    }
    
    func goToCustomAndShareController(withImage selectedImage: UIImage) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let customAndShareController = storyboard.instantiateViewController(withIdentifier: "CustomAndShareViewController") as! CustomAndShareViewController
        customAndShareController.currentImage = selectedImage
        
        self.navigationController?.pushViewController(customAndShareController, animated: true)
        
    }

}

extension HomeViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let sdImageCache = SDImageCache.shared()
        sdImageCache?.clearMemory()
        sdImageCache?.clearDisk()
        self.dismiss(animated: true, completion: nil)
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let newImg = AppManager.sharedInstance.fixrotation(for: image)
            self.goToCustomAndShareController(withImage: newImg)
            
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
