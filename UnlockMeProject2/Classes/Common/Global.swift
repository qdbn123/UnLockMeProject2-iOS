//
//  Global.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/25/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

class Global: NSObject {
    static var user: User?
    static var image: UIImage?
}
