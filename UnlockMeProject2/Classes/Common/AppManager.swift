//
//  AppDelegate.swift
//  EasyOut
//
//  Created by Giap Nguyen on 11/16/16.
//  Copyright © 2016 WeWork. All rights reserved.
//

import UIKit
import ReachabilitySwift

let IS_IPHONE = (UIDevice.current.userInterfaceIdiom == .phone)
let IS_IPAD = (UIDevice.current.userInterfaceIdiom == .pad)

let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height
let SCREEN_MAX_LENGTH = (max(SCREEN_WIDTH, SCREEN_HEIGHT))
let SCREEN_MIN_LENGTH = (min(SCREEN_WIDTH, SCREEN_HEIGHT))

let IS_IPHONE_4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
let IS_IPHONE_5 = (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
let IS_IPHONE_6 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
let IS_IPHONE_6P = (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
let IS_IPHONE_7 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
let IS_IPHONE_7P = (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

class AppManager {
    static let sharedInstance = AppManager()
    static let screenSize = UIScreen.main.bounds
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)
    static var homeNavigation: UINavigationController?
    
    func hideKeyboardWhenTappedAround(view: UIView) {
        let t = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard(tap:)))
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(t)
    }
    
    @objc private func dismissKeyboard(tap: UITapGestureRecognizer) {
        tap.view?.endEditing(true)
        
    }

    // MARK: - Loading
    func checkInternet()-> Bool{
        let reachability = Reachability()!
        if reachability.currentReachabilityStatus == .notReachable {
            self.showMessage(message: "Internet unavailable")
            return false
        }else {
            return true
        }
    }
    
    func showMessage(message: String, type: AlertStyle = .none) {
        _ = SweetAlert().showAlert(message)
    }
    
    func fixrotation(for image: UIImage) -> UIImage{
        
        
        if image.imageOrientation == .up {
            return image
        }
        var transform = CGAffineTransform.identity
        
        switch image.imageOrientation {
        case .down, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: image.size.height);
            transform = transform.rotated(by: CGFloat(M_PI));
            break;
            
        case .left, .leftMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0);
            transform = transform.rotated(by: CGFloat(M_PI_2));
            break
            
        case .right, .rightMirrored:
            transform = transform.translatedBy(x: 0, y: image.size.height);
            transform = transform.rotated(by: -(CGFloat)(M_PI_2));
            break
        case .up, .upMirrored:
            break
        }
        
        switch (image.imageOrientation) {
        case .upMirrored, .downMirrored:
            transform = transform.translatedBy(x: image.size.width, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            break;
            
        case .leftMirrored, .rightMirrored:
            transform = transform.translatedBy(x: image.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
            break;
        case .up: break
        case .down: break
        case .left: break
        case .right:
            break;
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx = CGContext(data: nil, width: Int(image.size.width), height: Int(image.size.height),
                            bitsPerComponent: image.cgImage!.bitsPerComponent, bytesPerRow: 0,
                            space: image.cgImage!.colorSpace!,
                            bitmapInfo: image.cgImage!.bitmapInfo.rawValue);
        ctx!.concatenate(transform);
        switch (image.imageOrientation) {
        case .left, .leftMirrored, .right, .rightMirrored:
            // Grr...
            ctx?.draw(image.cgImage!, in: CGRect(x: 0,y: 0,width: image.size.height, height: image.size.width))
            break;
            
        default:
            ctx?.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
            break;
        }
        
        // And now we just create a new UIImage from the drawing context
        let cgimg = ctx!.makeImage()
        let img: UIImage = UIImage(cgImage: cgimg!)
        
        return img
    }
    
    func textToImage(drawText: NSString, inImage: UIImage, atPoint:CGPoint)->UIImage{
        
        // Setup the font specific variables
        let textColor: UIColor = UIColor.white
        let textFont: UIFont =  UIFont.boldSystemFont(ofSize: 150)
        let textStyle  = NSTextEffectLetterpressStyle
        
        //Setup the image context using the passed image.
        UIGraphicsBeginImageContext(inImage.size)
        
        let textFontAttributes = [
            NSFontAttributeName: textFont,
            NSForegroundColorAttributeName: textColor,
            NSTextEffectAttributeName : textStyle
        ] as [String : Any]
        
        inImage.draw(in: CGRect(x: 0, y: 0, width: inImage.size.width, height: inImage.size.height))
        
        let textSize = drawText.size(attributes: textFontAttributes)
        let textRect = CGRect(x: inImage.size.width / 2 - textSize.width / 2,
                              y: inImage.size.height / 2 - textSize.height / 2,
                              width: inImage.size.width / 2 + textSize.width / 2,
                              height: inImage.size.height - textSize.height)
        drawText.draw(in: textRect, withAttributes: textFontAttributes)
        
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        
        UIGraphicsEndImageContext()
        return newImage
    }
}

