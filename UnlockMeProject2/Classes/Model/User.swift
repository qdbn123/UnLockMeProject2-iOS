
//
//  User.swift
//  UnlockMeProject2
//
//  Created by Macintosh on 3/25/17.
//  Copyright © 2017 Macintosh. All rights reserved.
//

import UIKit

class User: NSObject {
    var id: String!
    var name: String = ""
    var thumnail: String = ""
    
    override init() {
        
    }
    
    init(withDic dic: Dictionary<String, Any>) {
        if let id = dic["id"] as? String {
            self.id = id
        }
        
        if let name = dic["name"] as? String {
            self.name = name
        }
        
        if let picture = dic["picture"] as? Dictionary<String, Any> {
            if let data = picture["data"] as? Dictionary<String, Any> {
                if let url = data["url"] as? String {
                    self.thumnail = url
                }
            }
        }
        
    }
}
